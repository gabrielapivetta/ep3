# PIZZARIA


## O que é o site?

Este site é um meio pelo qual clientes podem escolher pizzas que desejam pedir e enviar para o servidor, onde funcionários recebem as informações do pedido de determinado cliente. Cada cliente também poderá escolher dentre diversos tipos de sabores e 3 tamanhos diferentes (pequeno = 1, médio = 2 e grande = 3).

## Qual é o objetivo?

O objetivo do site é facilitar a escolha de pizzas para os clientes e a listagem de pedidos para os funcionários. Uma vez que se têm em mãos as informações de cada cliente e o exato pedido de cada um, a probabilidade de erro de entrega se reduz. Logo, ganha-se em organização, facilidade, praticidade e eficiência.

## Quais são as funcionalidades do sistema?

1. O cliente consegue fazer SignUp, Login e Logout;
2. O cliente consegue escolher pizzas para comprar (uma de cada vez), de sabores e tamanhos diferentes;
3. O funcionário consegue acessar as listas de usuários, pizzas e sabores e editá-los, criá-los e apagá-los (através do link de acesso dos admins "localhost:3000/admin");
4. O cliente pode visualizar, apagar e editar os seus pedidos.

obs: Os sabores das pizzas (Toppings) podem ser criados pelo console (rails c).

## Como acessar o sistema?

1. Ter o ruby (versão 2.5.1) e o rails (versão 6.0.1) baixados no computador;
2. Baixar a pasta do repositório no qual se encontra;
3. Para melhor visualização do código, é indicado entrar por meio de uma máquina virtual, no caso, a utilizada para este projeto foi o VSCode;
4. Ir para o terminal e acessar a pasta na qual este arquivo foi salvo;
5. Escrever "rails s" (no terminal) e entrar pela internet em "localhost:3000";
6. Escrever "rails db:migrate" (se necessário), para que todas as migrações fiquem corretas antes de acessar o sistema;
7. Usufruir das funcionalidades do sistema!

## Quais as ferramentas utilizadas?

1. Ruby (versão 2.5.1) e Rails (versão 6.0.1);
2. As gems Devise, Pundit e Administrate para gerenciar as funcionalidades de usuário;
3. A aplicação Lucidchart para fazer os diagramas de Classes e de Casos de Uso;
4. HTML e CSS para a interface;
5. A máquina virtual VSCode para a elaboração do código;

## Diagrama de Casos de Uso

![](app/assets/images/casosdeuso.jpeg)

## Diagrama de Classes

![](app/assets/images/diagramadeclasses.jpeg)