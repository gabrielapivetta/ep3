Rails.application.routes.draw do
  namespace :admin do
      resources :users
      resources :pizzas
      resources :toppings

      root to: "users#index"
    end
  resources :pizzas
  resources :toppings
  devise_for :users
  root to: "pizzas#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
