class Pizza < ApplicationRecord
  belongs_to :topping
  belongs_to :user
  validates :size, presence: true
  validates_inclusion_of :size, :in => 1..3
end
