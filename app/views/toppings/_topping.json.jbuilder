json.extract! topping, :id, :kind, :price, :created_at, :updated_at
json.url topping_url(topping, format: :json)
